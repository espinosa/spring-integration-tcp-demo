package org.bitbucket.espinosa.demo.sitcp.heartbeat.server;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.integration.channel.DirectChannel;
import org.springframework.integration.channel.QueueChannel;
import org.springframework.integration.config.EnableIntegration;
import org.springframework.integration.dsl.IntegrationFlow;
import org.springframework.integration.dsl.IntegrationFlows;
import org.springframework.integration.ip.dsl.Tcp;
import org.springframework.integration.ip.tcp.TcpReceivingChannelAdapter;
import org.springframework.integration.ip.tcp.TcpSendingMessageHandler;
import org.springframework.integration.ip.tcp.connection.TcpNetServerConnectionFactory;
import org.springframework.integration.ip.tcp.serializer.ByteArrayLengthHeaderSerializer;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.PollableChannel;

@Configuration
@EnableIntegration
public class HeartbeatServerConfig {

	@Bean
	public MessageChannel outboudChannel() {
		return new DirectChannel();
	}

	@Bean
	public PollableChannel inboundChannel() {
		return new QueueChannel();
	}

	@Bean
	public TcpNetServerConnectionFactory connectionFactory() {
		TcpNetServerConnectionFactory connectionFactory = new TcpNetServerConnectionFactory(7777);
		connectionFactory.setSerializer(new ByteArrayLengthHeaderSerializer());
		connectionFactory.setDeserializer(new ByteArrayLengthHeaderSerializer());
		return connectionFactory;
	}

	@Bean
	public TcpReceivingChannelAdapter heartbeatReceivingMessageAdapter(
			TcpNetServerConnectionFactory connectionFactory,
			MessageChannel outboudChannel) {
		TcpReceivingChannelAdapter heartbeatReceivingMessageAdapter = new TcpReceivingChannelAdapter();
		heartbeatReceivingMessageAdapter.setConnectionFactory(connectionFactory);
		heartbeatReceivingMessageAdapter.setOutputChannel(outboudChannel);
		return heartbeatReceivingMessageAdapter;
	}

	@Bean
	public TcpSendingMessageHandler heartbeatSendingMessageHandler(
			TcpNetServerConnectionFactory connectionFactory) {
		TcpSendingMessageHandler heartbeatSendingMessageHandler = new TcpSendingMessageHandler();
		heartbeatSendingMessageHandler.setConnectionFactory(connectionFactory);
		return heartbeatSendingMessageHandler;
	}

	@Bean
	public IntegrationFlow heartbeatServerFlow(
			TcpReceivingChannelAdapter heartbeatReceivingMessageAdapter,
			TcpSendingMessageHandler heartbeatSendingMessageHandler, 
			MessageChannel outboudChannel,
			PollableChannel inboundChannel) {
		return IntegrationFlows // ??????????????????????????????????????????
				.from(Tcp.inboundGateway(Tcp.netServer(7777)).requestChannel(inboundChannel))
				.channel(outboudChannel)
				.get();
	}

	@Bean
	public HeartbeatServer heartbeatServer(
			PollableChannel inboundChannel, 
			MessageChannel outboudChannel) {
		return new HeartbeatServer(inboundChannel, outboudChannel);
	}
}
