# Simple Echo Socket Tcp/Ip Service in Spring Integration DSL

Example implementation of a simple client/server _echo_ service in Spring Integration DSL and Tcp/Ip socket connection.
My Heartbeat service is a simple server Spring Boot application waiting for client to ask "status", responding with "OK".

See: https://stackoverflow.com/questions/55154418/how-to-implement-simple-echo-socket-service-in-spring-integration-dsl

This project examines how to plug Adapters, Handlers and Gateways to IntegrationFlows on the client and server side. Practical examples are hard to come by for Spring Integration DSL and TCP/IP client/server communication. There is an sample echo service in SI examples, but it's written in the "old" XML configuration and I really struggle to transform it to the configuration by code.
