package org.bitbucket.espinosa.demo.sitcp.heartbeat.client;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.integration.channel.DirectChannel;
import org.springframework.integration.channel.QueueChannel;
import org.springframework.integration.config.EnableIntegration;
import org.springframework.integration.dsl.IntegrationFlow;
import org.springframework.integration.dsl.IntegrationFlows;
import org.springframework.integration.ip.dsl.Tcp;
import org.springframework.integration.ip.tcp.TcpReceivingChannelAdapter;
import org.springframework.integration.ip.tcp.TcpSendingMessageHandler;
import org.springframework.integration.ip.tcp.connection.TcpNetClientConnectionFactory;
import org.springframework.integration.ip.tcp.serializer.ByteArrayLengthHeaderSerializer;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.PollableChannel;

@Configuration
@EnableIntegration
public class HeartbeatClientConfig {

	@Bean
	public MessageChannel outboudChannel() {
		return new DirectChannel();
	}

	@Bean
	public PollableChannel inboundChannel() {
		return new QueueChannel();
	}

	@Bean
	public TcpNetClientConnectionFactory connectionFactory() {
		TcpNetClientConnectionFactory connectionFactory = new TcpNetClientConnectionFactory("localhost", 7777);
		connectionFactory.setSerializer(new ByteArrayLengthHeaderSerializer());
		connectionFactory.setDeserializer(new ByteArrayLengthHeaderSerializer());
		return connectionFactory;
	}

	@Bean
	public TcpReceivingChannelAdapter heartbeatReceivingMessageAdapter(
			TcpNetClientConnectionFactory connectionFactory,
			MessageChannel inboundChannel) {
		TcpReceivingChannelAdapter heartbeatReceivingMessageAdapter = new TcpReceivingChannelAdapter();
		heartbeatReceivingMessageAdapter.setConnectionFactory(connectionFactory);
		heartbeatReceivingMessageAdapter.setOutputChannel(inboundChannel); // ???
		heartbeatReceivingMessageAdapter.setClientMode(true);
		return heartbeatReceivingMessageAdapter;
	}

	@Bean
	public TcpSendingMessageHandler heartbeatSendingMessageHandler(
			TcpNetClientConnectionFactory connectionFactory) {
		TcpSendingMessageHandler heartbeatSendingMessageHandler = new TcpSendingMessageHandler();
		heartbeatSendingMessageHandler.setConnectionFactory(connectionFactory);
		return heartbeatSendingMessageHandler;
	}

	@Bean
	public IntegrationFlow heartbeatClientFlow(
			TcpNetClientConnectionFactory connectionFactory,
			TcpReceivingChannelAdapter heartbeatReceivingMessageAdapter,
			TcpSendingMessageHandler heartbeatSendingMessageHandler,
			MessageChannel outboudChannel,
			PollableChannel inboundChannel
			) {
		return IntegrationFlows // ??????????????????????????????????????????
				.from(outboudChannel)
				//.handle(Tcp.outboundGateway(connectionFactory)) // ..interestingly, using 'connectionFactory' doesn't work but defining new one does
				.handle(Tcp.outboundGateway(Tcp.netClient("localhost", 7777)))
				.channel(inboundChannel)
				.get();
	}

	@Bean
	public HeartbeatClient heartbeatClient(
			MessageChannel outboudChannel,
			PollableChannel inboundChannel) {
		return new HeartbeatClient(outboudChannel, inboundChannel);
	}
}
