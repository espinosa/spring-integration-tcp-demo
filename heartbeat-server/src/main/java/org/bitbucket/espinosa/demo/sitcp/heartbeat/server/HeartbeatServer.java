package org.bitbucket.espinosa.demo.sitcp.heartbeat.server;

import java.util.concurrent.Executors;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.PollableChannel;
import org.springframework.messaging.support.GenericMessage;

public class HeartbeatServer {
	private final PollableChannel inboundChannel;
	private final MessageChannel outboudChannel;
	private final Logger log = LogManager.getLogger(HeartbeatServer.class);

	public HeartbeatServer(PollableChannel inboundChannel, MessageChannel outboudChannel) {
		this.inboundChannel = inboundChannel;
		this.outboudChannel = outboudChannel;
	}

	@EventListener
	public void initializaAfterContextIsReady(ContextRefreshedEvent event) {
		log.info("Starting Heartbeat");
		start();
	}

	public void start() {
		Executors.newSingleThreadExecutor().execute(() -> {
			while (true) {
				try {
					Message<?> message = inboundChannel.receive(1000);
					if (message == null) {
						log.error("Heartbeat timeouted");
					} else {
						String messageStr = new String((byte[]) message.getPayload());
						if (messageStr.equals("status")) {
							log.info("Heartbeat received");
							outboudChannel.send(new GenericMessage<>("OK"));
						} else {
							log.error("Unexpected message content from client: " + messageStr);
						}
					}
				} catch (Exception e) {
					log.error(e);
				}
			}
		});
	}
}
