package org.bitbucket.espinosa.demo.sitcp.heartbeat.server;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HeartbeatServerRun {
	public static void main(String[] args) {
		SpringApplication.run(HeartbeatServerConfig.class, args);
	}
}
